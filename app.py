#	seen, flask edition - A simple blog generator.
#	Copyright (C) 2022 Ferass EL HAFIDI
#
#	This program is free software: you can redistribute it and/or modify
#	it under the terms of the GNU Affero General Public License as published by
#	the Free Software Foundation, either version 3 of the License, or
#	(at your option) any later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU Affero General Public License for more details.
#
#	You should have received a copy of the GNU Affero General Public License
#	along with this program.  If not, see <https://www.gnu.org/licenses/>.
import glob
import os
from flask import Flask
from flask import render_template
from flask import abort
from flask.wrappers import Response
from markupsafe import Markup
import markdown
import configparser
app = Flask(__name__)

@app.route('/')
def index():
	"""
	This is the article index function. It generates a list of articles.
	"""
	config = configparser.ConfigParser()
	config.read('config.ini')
	articles = "<div class=\"articles\">"
	# Get config values such as the Blog name and the Welcome text.
	try:
		blog_name = config['Blog']['Name']
		description = config['Blog']['Description']
		footer = config['Blog']['Footer']
	except KeyError:
		return "<code>Missing configuration! Please configure the config.ini!</code>"
	# Get a list of all articles.
	files = glob.glob("articles/*.md")
	files.sort(key=os.path.getctime)
	for file in files[::-1]:
		try: 
			articles += render_template('list.html', file = file.replace(".md", ""), 
				date = config[file.replace(".md", "").replace("articles/", "")]['Date'], 
				article = config[ \
					file.replace(".md", "").replace("articles/", "")]['Title'])
		except KeyError: 
			return "<code>No config.ini entry! " + \
			"Please create a config.ini entry for the %s article!</code>" % file
	articles += "\n</div>"
	# Return index.
	return render_template('template.html', blog_name = blog_name, 
		welcome_text = description, footer = footer, article_index = Markup(articles))

@app.route('/articles/')
@app.route('/articles/<name>')
def article(name = None):
	"""
	This is the article function. It parses a markdown file, converts it to 
	html, and formats it.
	"""
	# Open the markdown file, complain if it doesn't exist.
	try: article_file = open("articles/" + name + ".md", mode='r')
	except OSError: abort(404)
	# Convert it to HTML5.
	article_text = Markup(markdown.markdown(article_file.read()))
	# Close the file
	article_file.close()
	# Override the defaults
	config = configparser.ConfigParser()
	config.read('config.ini')
	try:
		article_title = config[name]['Title']
		article_date = config[name]['Date']
		article_description = config[name]['Description']
		footer = config['Blog']['Footer']
	except KeyError:
		return "<code>Missing configuration! Please configure the config.ini!</code>"
	# Finally, return the article text.
	return render_template('template.html', article = True, article_title = article_title, 
		article_text = article_text, article_description = article_description, 
		article_date = article_date, footer = footer)

@app.route('/rss.xml')
def rss_feeds():
	"""
	This function generates RSS Feeds.
	"""
	config = configparser.ConfigParser()
	config.read('config.ini')
	if config['Blog']['RSS'] != 'y': abort(404)
	blog_name = config['Blog']['Name']
	try:
		blog_description = config['Blog']['Description']
		rss_feed = "<rss version=\"2.0\">"
		rss_feed += "<channel>" + "<title>" + blog_name + "</title>"
		rss_feed += "<link>" + config['Blog']['URL'] + "</link>"
		rss_feed += "<description>" + blog_description + "</description>"
	except KeyError:
		return "<code>Missing configuration! Please configure the config.ini!</code>"
	# Create an RSS entry for each article.
	for file in glob.glob("articles/*.md"):
		article = open(file, mode='r')
		rss_feed += "<item>"
		rss_feed += "<title>" + \
			config[file.replace("articles/", "").replace(".md", "")]['Title'] + \
			"</title>"
		rss_feed += "<description><![CDATA[" + escape(article.read()) + "]]></description>"
		rss_feed += "</item>"
		article.close()
	rss_feed += "</channel>" + "</rss>"
	# Return it!
	return Response(rss_feed, mimetype='application/xml')


Welcome! Today, we're going to learn how to setup a simple and lightweight git server!

We're gonna do the following :

- Install required software

- Configure SSH

- Configure Git & Gitolite

- Configure NginX

- Configure GitWeb


> Please note that you **have** to replace the "server" hostname with your ip address

## Installing required software

All we need here is git, gitolite and gitweb:

	root:server# apt install git gitolite3 gitweb

(if it says to create a user or configure something, say "no", we are gonna configure everything later!)

## Configuring 

### ssh

It is **strongly recommended** to use ssh keys to log in instead of using a password.

#### Generate a key

It's actually pretty simple, just run the following **in your computer** (not the server) :

	user:computer$ ssh-keygen

And follow the instructions.

#### Make your server trust it

- 1st method

Just run :

	user:computer$ ssh-copy-id root@server

- 2nd method

Go to your ssh **public** key and copy everything in there. Ssh into the server and paste your ssh public key to your ~/.ssh/authorized_keys.

#### Disable password logins

Now, if everything happened successfully, you can now log into your server without the need of the user's password (but if you entered an ssh key password, you will be promped to enter ***that*** password instead)! Let's now disable password logins for security reasons!

Log into your server and open the /etc/sshd/sshd_config file. Now find, uncomment or create these lines and set them to *no* :

	root:server# vim /etc/sshd/sshd_config


	PasswordAuthentication no

	ChallengeResponseAuthentication no

	UsePAM no

Now reload sshd:

	root:server# systemctl restart sshd

Done! Now our server is more secure than ever!

### Git and Gitolite

Gitolite is a nice software to manage our repos. There is Gitosis too but I prefer Gitolite.

#### Create a "git" account

For this, we need to create an account named "git". This account will be used for managing our repos.

Just run the following:

	root:server# sudo adduser \
	--system \
	--shell /bin/bash \
	--gecos 'git SCM user' \
	--group \
	--disabled-password \
	--home /var/lib/git \
	git

This will create our git account, with the full name "git SCM user", no password and with the /var/lib/git home directory. It will be a **system** user.

#### Gitolite

Now, lets configure Gitolite. As the "git" user, create the "sshkeys" directory (in /var/lib/git) and copy the ssh **public** key we created earlier there.

Once that's done, run the following:

	git:server$ gitolite setup -pk sshkeys/<key goes here>

Now, we need to configure the right permissions, or else, gitweb won't work.

Use your [favourite text editor](http://vim.org) to edit the ~/.gitolite.rc file and change the UMASK to 0027:

	git:server$ vim .gitolite.rc

	[...]
	# default umask gives you perms of '0700'; see the rc file docs for
	# how/why you might change this
	UMASK                           =>  0027,
	[...]

And after that, run as root:

	root:server# chmod g+rX /var/lib/git && chmod -R g+rX /var/lib/git/repositories

Done! Now Gitolite is configured!

#### Create repositories

***Do NOT add new repos or users manually on the server!!!***

To create a repository, run the following on your computer (Here, I'm cloning it on /tmp/):

	user:computer$ cd /tmp && git clone git@server:gitolite-admin.git 

After that, use your text editor to edit the conf/gitolite.conf. Add the following:


	user:computer$ vim gitolite-admin/conf/gitolite.conf

	repo {repo name goes here}

		RW+		= <username>


For more info, gitolite has **very good** documentation [here](https://gitolite.com).

### NginX (pronounced "Engine X")

Now, lets configure nginX!
Create the /etc/nginx/sites-available/git_server and add the following :

	root:server# vim /etc/nginx/sites-available/git_server

	server {
		#replace "git.example.com" below with your domain (or subdomain)
		server_name git.example.org;
		listen 80;
		root /usr/lib/cgi-bin;
		location /index.cgi {
			root /usr/share/gitweb/;
			include fastcgi_params;
			gzip off;
			fastcgi_param SCRIPT_NAME $uri;
			fastcgi_param GITWEB_CONFIG /etc/gitweb.conf;
			fastcgi_pass  unix:/var/run/fcgiwrap.socket;
		}
		
		location / {
			root /usr/share/gitweb/;
			index index.cgi;
		}
		
	}

After that, we need to add the "www-data" user to the git group (permissions...):

> Note : in this case, the user nginX uses is "www-data" but sometimes in other distributions, it is "nginx" or "http".

	root:server# usermod -aG git www-data # Change www-data with http or nginx for some other distros

Now, restart nginX:

	root:server# systemctl restart nginx

Go to your browser and you will see glourious gitweb working!

### GitWeb

Now, let's configure gitweb!

If you created repos on Gitolite, these repos won't show up on gitweb because we didn't define where to search them.

Edit the /etc/gitweb.conf file and add/modify the following:

	root:server# vim /etc/gitweb.conf

	[...]
	$projects_list = "/var/lib/git/projects.list";
	$projectroot = "/var/lib/git/repositories";
	[...]

Done! Now your repos will appear in git.example.org

### CUSTOMIZATIONS !1!!!!!111!!1!1

Did you know you can customize GitWeb? Take a look at my Git Server at vitali64.duckdns.org. As you can see, I applied my *own* stylesheet.

How you can customize it:

the CSS is located in : /usr/share/gitweb/static/gitweb.css

cgi scripts are located in : /usr/share/gitweb/gitweb.cgi and /usr/share/gitweb/index.cgi

the config file is located in : /etc/gitweb.conf

## Conclusion

Well, I hope this helped you setting up a git server. If you found a mistake or anything, you can contact me on Matrix : @notrealvitali64:matrix.org.

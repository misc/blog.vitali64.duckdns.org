I will stop using gitlab as my main git server and will be moving on my own git server located here : vitali64.duckdns.org.

Articles and Website will still be hosted on GitLab.


I already archived every repo (except Articles and Website).


EDIT - 3rd October 2022:

I'm not using GitLab for anything anymore (not even for articles or websites) 
as I'm self-hosting mostly everything: Git, the website, ... .

https://vitali64.gitlab.io will soon be a redirect to 
https://vitali64.duckdns.org and https://vitali64.gitlab.io/articles/ will redirect 
to https://blog.vitali64.duckdns.org .


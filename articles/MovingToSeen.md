
You all know that I have a blog, and I post things there.

Well, this blog used blogit, blogit is a static blog generator,
it uses git for that.

Well, recently, I made my own blog generator called `seen`:

- It's POSIX-compliant (in fact, written in POSIX sh) ;

- Less than 100 SLOC ;

- Fast ;

- GPLv3-licensed.

Now, I decided to use that as my blog generator instead, one problem is:

- It *can* generate RSS feeds, but it's currently buggy.

Feel free to use it and modify it, here's the [source code](https://vitali64.duckdns.org/?p=utils/seen.git;a=summary)
